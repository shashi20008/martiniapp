package com.martini;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    String dashboardJSON;
    public PagerAdapter(FragmentManager fm, int NumOfTabs, String dashboardJSON) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.dashboardJSON = dashboardJSON;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle b = new Bundle();
        b.putString("json", dashboardJSON);
        switch (position) {
            case 0:
                HomeFragment tab1 = new HomeFragment();
                tab1.setArguments(b);
                return tab1;
            case 1:
                TrendsFragment tab2 = new TrendsFragment();
                tab2.setArguments(b);
                return tab2;
            case 2:
                ReportsFragment tab3 = new ReportsFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
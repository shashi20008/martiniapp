package com.martini;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ReportsFragment extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reports_fragment, container, false);
        final Button dateButton = (Button)view.findViewById(R.id.date_button);
        dateButton.setText("Choose Date Range");
        // Show a datepicker when the dateButton is clicked
        final Fragment f = this;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();


                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                             @Override
                             public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                                 monthOfYear++;
                                 monthOfYearEnd++;
                                 dateButton.setText(dayOfMonth + "/" + monthOfYear + "/" + year + " - " + dayOfMonthEnd + "/" + monthOfYearEnd + "/" + yearEnd);

                                 SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                 Date date = null;
                                 try {
                                     long startDate = format.parse(dayOfMonth + "/" + monthOfYear + "/" + year).getTime();
                                     long endDate = format.parse(dayOfMonthEnd + "/" + monthOfYearEnd + "/" + yearEnd).getTime();
                                     new FetchReportsTask(f).execute(preferences.getString("SERVER","") + "/report?startDate=" + startDate + "&endDate=" + endDate + "&user=" + preferences.getString("USER",""));
                                 } catch (ParseException e) {
                                     e.printStackTrace();
                                 }
                             }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                FragmentManager fm = getActivity().getFragmentManager();
                dpd.show(fm, "Datepickerdialog");
            }
        });
        return view;
    }

    private class FetchReportsTask  extends AsyncTask<String, Void, Void> {

        private final HttpClient Client = new DefaultHttpClient();
        private String Content;
        private String Error = null;
        private Fragment container;
        private TextView uiUpdate;
        TableLayout reportTable ;
        public FetchReportsTask(Fragment f) {
            this.container = f;
            this.uiUpdate = (TextView) f.getView().findViewById(R.id.sample_text);
            this.reportTable = (TableLayout) f.getView().findViewById(R.id.report_table);
        }
        protected void onPreExecute() {
            // NOTE: You can call UI Element here.
            uiUpdate.setText("Loading..");
        }

        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(urls[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                Content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

            // Close progress dialog

            if (Error != null) {

                uiUpdate.setText("Error : " + Error);

            } else {

                uiUpdate.setText("");
                int i = 0;
                reportTable.removeAllViews();
                try {
                    JSONObject  response = new JSONObject(Content);
                    Iterator<?> keys = response.keys();

                    while( keys.hasNext() ) {
                        String key = (String)keys.next();
                        TableRow tr = new TableRow(this.container.getContext());
                        tr.setId(100 + i);
                        TableLayout.LayoutParams tlp = new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.MATCH_PARENT,
                                TableLayout.LayoutParams.WRAP_CONTENT);
                        tr.setPadding(0,15,0,15);
                        tr.setLayoutParams(tlp);
                        //tr.setBackground(getResources().getDrawable(R.drawable.stroke));
                        // Create a TextView to house the name of the province
                        TextView dateTV = new TextView(this.container.getContext());
                        dateTV.setId(200+i);
                        dateTV.setText((String)key);
                        dateTV.setTextSize(18.0f);
                        dateTV.setPadding(15, 0, 0, 0);
                        dateTV.setTextColor(Color.BLACK);
                        tr.addView(dateTV);

                        // Create a TextView to house the value of the after-tax income
                        TextView usageTV = new TextView(this.container.getContext());
                        usageTV.setId(i);
                        usageTV.setText(new Integer((int)response.get(key)).toString());
                        usageTV.setGravity(Gravity.RIGHT);
                        usageTV.setPadding(0,0, 60, 0);
                        usageTV.setTypeface(null, Typeface.BOLD);
                        usageTV.setTextSize(18.0f);
                        usageTV.setTextColor(Color.BLACK);
                        tr.addView(usageTV);
                        TableRow br = new TableRow(this.container.getContext());
                        br.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.stroke, null));
                        // Add the TableRow to the TableLayout

                        reportTable.addView(tr, new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.MATCH_PARENT,
                                TableLayout.LayoutParams.WRAP_CONTENT));
                        reportTable.addView(br, new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.FILL_PARENT,
                                TableLayout.LayoutParams.WRAP_CONTENT));
                        reportTable.requestLayout();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }
}


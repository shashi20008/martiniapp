package com.martini;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 5000;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private static String SERVER = "http://192.168.1.5:3000";
    private static String USERNAME = "martiniweb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("SERVER", SERVER);
        editor.putString("USERNAME", USERNAME);
        editor.commit();
        setContentView(R.layout.activity_splash_screen);
        new FetchDashboardData(this).execute(preferences.getString("SERVER", "") + "?user=" + preferences.getString("USERNAME", ""));
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private class FetchDashboardData extends AsyncTask<String, Void, Void> {
        Activity splashActivity;
        private final HttpClient Client = new DefaultHttpClient();
        private String content;
        private String Error = null;

        public FetchDashboardData(Activity splashActivity) {
            this.splashActivity = splashActivity;
        }

        @Override
        protected Void doInBackground(String... url) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(url[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                System.out.println("########"+Error);
            } catch (IOException e) {
                Error = e.getMessage();
                System.out.println("########"+Error);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            System.out.println("$$$$$$$$$$$$");
            if (Error != null) {
                System.out.println(Error);
                Toast.makeText(splashActivity.getApplicationContext(), "Error connecting to server.", Toast.LENGTH_LONG).show();
            } else {
                // NOTE: You can call UI Element here.
                Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                Bundle b = new Bundle();
                b.putString("dashboardJSON", content);
                i.putExtras(b);
                startActivity(i);
                // Close progress dialog
                splashActivity.finish();
            }

        }
    }

}
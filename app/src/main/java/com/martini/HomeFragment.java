package com.martini;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        String json = getArguments().getString("json");
        try {
            JSONObject response = new JSONObject(json);
            TextView dailyAvg_consumption_value = (TextView) view.findViewById(R.id.dailyAvg_consumption_value);
            dailyAvg_consumption_value.setText(response.getString("avgDailyConsumption"));
            TextView monthlyAvg_consumption_value = (TextView) view.findViewById(R.id.monthlyAvg_consumption_value);
            monthlyAvg_consumption_value.setText(response.getString("avgMonthlyConsumption"));
            TextView monthly_consumption_value = (TextView) view.findViewById(R.id.monthly_consumption_value);
            monthly_consumption_value.setText(response.getString("monthConsumption"));
            TextView today_consumption_value = (TextView) view.findViewById(R.id.today_consumption_value);
            today_consumption_value.setText(response.getString("todayConsumption"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FloatingActionButton train = (FloatingActionButton) view.findViewById(R.id.train);
        final Fragment f = this;
        train.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(f.getContext(), ShowDevices.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
package com.martini;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public String getChartsSettings(String chartType){
        return "{\"data\": { \"columns\": [ [\"yo\", 30, 200, 100, 400, 150, 250], [\"yo2\", 50, 20, 10, 40, 15, 25] ]}}";
    }
}
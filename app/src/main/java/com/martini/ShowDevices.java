package com.martini;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONArray;

import java.io.IOException;

public class ShowDevices extends AppCompatActivity {

    private GoogleApiClient client;
    JSONArray deviceData;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;


    protected void onStart(Bundle savedInstanceState) {
        System.out.println("inside on start");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        new FetchDevice(this).execute(preferences.getString("SERVER", "") + "?user=" + preferences.getString("USERNAME", ""));
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_devices);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button addDevice = (Button) findViewById(R.id.add_device);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        new FetchDevice(this).execute(preferences.getString("SERVER", "") + "?user=" + preferences.getString("USERNAME", ""));
        final Context c = this.getBaseContext();
        addDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(c, AddDevice.class);
                startActivity(intent);
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowDevices Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.martini/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowDevices Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.martini/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }

    private class FetchDevice extends AsyncTask<String, Void, Void> {
        Activity showDevicesActivity;
        private final HttpClient Client = new DefaultHttpClient();
        private String content;
        private String Error = null;

        public FetchDevice(Activity showDevicesActivity) {
            this.showDevicesActivity = showDevicesActivity;
        }

        @Override
        protected Void doInBackground(String... url) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(url[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            if (Error != null) {
                System.out.println(Error);
                Toast.makeText(showDevicesActivity.getApplicationContext(), "Error connecting to server.", Toast.LENGTH_SHORT).show();
            }
            try {
                deviceData = new JSONArray(content);
                System.out.println(deviceData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < deviceData.length(); i++) {
                CardView cv = (CardView) getLayoutInflater().inflate(R.layout.added_device, null);
                try {
                    final String deviceName = deviceData.getJSONObject(i).getString("name");
                    final String deviceType = deviceData.getJSONObject(i).getString("type");
                    ((TextView) cv.findViewById(R.id.device_name)).setText(deviceName);
                    ((TextView) cv.findViewById(R.id.device_type)).setText(deviceType);
                    cv.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            Intent I = new Intent(showDevicesActivity, AddDevice.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("deviceName",deviceName );
                            bundle.putString("deviceType", deviceType);
                            I.putExtra("deviceData", bundle);
                            startActivity(I);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((LinearLayout) findViewById(R.id.added_device)).addView(cv);
            }
            // NOTE: You can call UI Element here.
            //Intent i = new Intent(ShowDevices.this, ShowDevices.class);
//            Bundle b = new Bundle();
//            System.out.println(content);
//            b.putString("deviceJSON", content);
//            i.putExtras(b);
//            startActivity(i);
//            // Close progress dialog
            //showDevicesActivity.finish();

        }
    }
}

package com.martini;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

public class AddDevice extends AppCompatActivity {
    private String deviceName;
    private String deviceType;
    private long startTime;
    private long endTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().hasExtra("deviceData")) {
            deviceName = getIntent().getBundleExtra("deviceData").getString("deviceName");
            deviceType = getIntent().getBundleExtra("deviceData").getString("deviceType");
            System.out.println("%%%%%%%%%" + deviceName + deviceType);
            ((EditText)this.findViewById(R.id.device_name)).setEnabled(false);
            ((EditText)this.findViewById(R.id.device_name)).setText(deviceName);
            ((RadioGroup)this.findViewById(R.id.device_type_radio_group)).removeAllViews();
        }
        final Button startTrainingButton = (Button) findViewById(R.id.start_training);
        final RelativeLayout rl = (RelativeLayout) findViewById(R.id.relative_layout);
        final Button endTraining = (Button)getLayoutInflater().inflate(R.layout.end_training_button, null);
        final Activity addDevice = this;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        endTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endTime = new Date().getTime();
                System.out.println("### End Training");
                TrainDeviceTask tdt = new TrainDeviceTask(deviceName, deviceType, startTime, endTime, addDevice, endTraining, preferences.getString("USERNAME", ""));
                tdt.execute(preferences.getString("SERVER","") + "/addDevice");
            }
        });
        final LinearLayout ll = (LinearLayout) findViewById(R.id.end_training_wrapper);
        startTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTime = new Date().getTime();
                deviceName = ((EditText)addDevice.findViewById(R.id.device_name)).getText().toString();

                int checkedId = ((RadioGroup)addDevice.findViewById(R.id.device_type_radio_group)).getCheckedRadioButtonId();
                if (checkedId > 0) {
                    deviceType = ((RadioButton)addDevice.findViewById(checkedId)).getText().toString();
                }
                if (!deviceName.equals("") && !deviceType.equals("")) {
                    ((EditText)addDevice.findViewById(R.id.device_name)).setEnabled(false);
                    RadioGroup rg = ((RadioGroup)addDevice.findViewById(R.id.device_type_radio_group));
                    for (int i = 0; i < rg.getChildCount(); i++) {
                        rg.getChildAt(i).setEnabled(false);
                    }
                    rl.removeView(startTrainingButton);
                    ll.addView(endTraining);
                }
            }
        });
    }

    private class TrainDeviceTask  extends AsyncTask<String, Void, Void> {

        private final HttpClient Client = new DefaultHttpClient();
        private String deviceName;
        private String deviceType;
        private long startTime;
        private long endTime;
        private Button endTrainingButton;
        private String Error = null;
        private Activity container;
        private String Content;
        private String user;
        public TrainDeviceTask(String deviceName, String deviceType, long startTime, long endTime, Activity container, Button endTrainingButton, String user) {
            this.container = container;
            this.deviceName = deviceName;
            this.deviceType = deviceType;
            this.startTime = startTime;
            this.endTime = endTime;
            this.user = user;
            this.endTrainingButton = endTrainingButton;
        }
        protected void onPreExecute() {
            // NOTE: You can call UI Element here.
            endTrainingButton.setText("Please wait..");
        }

        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                JSONObject deviceDetails = new JSONObject();
                deviceDetails.put("user", this.user);
                deviceDetails.put("startDate",this.startTime);
                deviceDetails.put("endDate", this.endTime);
                deviceDetails.put("deviceName",this.deviceName);
                deviceDetails.put("deviceType", this.deviceType);

                System.out.println("$$$$$$$" + deviceDetails);

                HttpPost request = new HttpPost(urls[0]);
                StringEntity params =new StringEntity(deviceDetails.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse  response = (Client.execute(request));
                System.out.println("#######" + response);
                Content = response.getStatusLine().getReasonPhrase();

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (JSONException e) {
                Error = e.getMessage();
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

            // Close progress dialog

            if (Error != null) {

                endTrainingButton.setText("Error : " + Error);

            } else {

                System.out.print(Content);
                finish();
                Intent intent = new Intent(container, ShowDevices.class);
                startActivity(intent);
            }
        }

    }
}
